import torch
from torch import nn
from utils import LevelMapper
from torchvision.ops import roi_align, roi_pool

class MultiScaleRoIAlign(nn.Module):
    def __init__(self, featmap_names, output_size, sampling_ratio):
        super(MultiScaleRoIAlign, self).__init__()
        if isinstance(output_size, int):
            output_size = (output_size, output_size)
        self.featmap_names = featmap_names
        self.sampling_ratio = sampling_ratio
        self.output_size = tuple(output_size)
        self.scales = None
        self.map_levels = None

    def convert_to_roi_format(self, boxes):
        ids = torch.arange(boxes.size(0)).view(-1, 1, 1).repeat(1, boxes.size(1), 1).to(boxes)
        rois = torch.cat([ids, boxes], dim = -1)
        return rois

    def infer_scale(self, feature_map, original_size):
        # assumption: the scale is of the form 2 ** (-k), with k integer
        size = feature_map.shape[-2:]
        possible_scales = []
        for s1, s2 in zip(size, original_size):
            approx_scale = float(s1) / s2
            scale = 2 ** torch.tensor(approx_scale).log2().round().item()
            possible_scales.append(scale)
        assert possible_scales[0] == possible_scales[1]
        return possible_scales[0]

    def setup_scales(self, feature_maps, image_size):
        scales = [self.infer_scale(feature_map, image_size) for feature_map in feature_maps]
        # get the levels in the feature map by leveraging the fact that the network always
        # downsamples by a factor of 2 at each level.
        lvl_min = -torch.log2(torch.tensor(scales[0], dtype=torch.float32)).item()
        lvl_max = -torch.log2(torch.tensor(scales[-1], dtype=torch.float32)).item()
        self.scales = scales
        self.map_levels = LevelMapper(lvl_min, lvl_max)

    def forward(self, feature_maps, proposals, image_size):
        feature_maps = [v for k, v in feature_maps.items() if k in map(type(k), self.featmap_names)]
        num_levels = len(feature_maps)
        rois = self.convert_to_roi_format(proposals)
        if self.scales is None:
            self.setup_scales(feature_maps, image_size)

        if num_levels == 1:
            align_kwargs = {
                'input': feature_maps[0],
                'boxes': rois.view(-1, 5),
                'output_size': self.output_size,
                'spatial_scale': self.scales[0],
                'sampling_ratio': self.sampling_ratio
            }
            return roi_align(**align_kwargs)

        levels = self.map_levels(proposals)

        result = torch.zeros((*rois.shape[:2], feature_maps[0].size(1), *self.output_size)).to(feature_maps[0])
        for level, (feature_map, scale) in enumerate(zip(feature_maps, self.scales)):
            idx_in_level = torch.nonzero(levels == level).unbind(dim = 1)
            align_kwargs = {
                'input': feature_map,
                'boxes': rois[idx_in_level],
                'output_size': self.output_size,
                'spatial_scale': scale,
                'sampling_ratio': self.sampling_ratio
            }
            result[idx_in_level] = roi_align(**align_kwargs)
        return result

class MultiScaleRoIPool(nn.Module):
    def __init__(self, featmap_names, output_size):
        super(MultiScaleRoIAlign, self).__init__()
        if isinstance(output_size, int):
            output_size = (output_size, output_size)
        self.featmap_names = featmap_names
        self.output_size = tuple(output_size)
        self.scales = None
        self.map_levels = None

    def convert_to_roi_format(self, boxes):
        ids = torch.arange(boxes.size(0)).view(-1, 1, 1).repeat(1, boxes.size(1), 1).to(boxes)
        rois = torch.cat([ids, boxes], dim = -1)
        return rois

    def infer_scale(self, feature_map, original_size):
        # assumption: the scale is of the form 2 ** (-k), with k integer
        size = feature_map.shape[-2:]
        possible_scales = []
        for s1, s2 in zip(size, original_size):
            approx_scale = float(s1) / s2
            scale = 2 ** torch.tensor(approx_scale).log2().round().item()
            possible_scales.append(scale)
        assert possible_scales[0] == possible_scales[1]
        return possible_scales[0]

    def setup_scales(self, feature_maps, image_size):
        scales = [self.infer_scale(feature_map, image_size) for feature_map in feature_maps]
        # get the levels in the feature map by leveraging the fact that the network always
        # downsamples by a factor of 2 at each level.
        lvl_min = -torch.log2(torch.tensor(scales[0], dtype=torch.float32)).item()
        lvl_max = -torch.log2(torch.tensor(scales[-1], dtype=torch.float32)).item()
        self.scales = scales
        self.map_levels = LevelMapper(lvl_min, lvl_max)

    def forward(self, feature_maps, proposals, image_size):
        feature_maps = [v for k, v in feature_maps.items() if k in map(type(k), self.featmap_names)]
        num_levels = len(feature_maps)
        rois = self.convert_to_roi_format(proposals)
        if self.scales is None:
            self.setup_scales(feature_maps, image_size)

        if num_levels == 1:
            pool_kwargs = {
                'input': feature_maps[0],
                'boxes': rois.view(-1, 5),
                'output_size': self.output_size,
                'spatial_scale': self.scales[0]
            }
            return roi_pool(**pool_kwargs)

        levels = self.map_levels(proposals)

        result = torch.zeros((*rois.shape[:2], feature_maps[0].size(1), *self.output_size)).to(feature_maps[0])

        for level, (feature_map, scale) in enumerate(zip(feature_maps, self.scales)):
            idx_in_level = torch.nonzero(levels == level).squeeze(1)
            pool_kwargs = {
                'input': feature_map,
                'boxes': rois[idx_in_level],
                'output_size': self.output_size,
                'spatial_scale': scale
            }
            result[idx_in_level] = roi_pool(**pool_kwargs)
        return result
