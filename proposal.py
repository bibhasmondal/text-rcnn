import torch
from torch import nn
import torch.nn.functional as F

class ProposalCreator(nn.Module):
    def __init__(self, box_coder, pre_nms_top_n, post_nms_top_n, nms_thresh, min_size = 1e-3):
        super(ProposalCreator, self).__init__()
        self.box_coder = box_coder
        self._pre_nms_top_n = pre_nms_top_n
        self._post_nms_top_n = post_nms_top_n
        self.min_size = min_size
        self.nms_thresh = nms_thresh

    @property
    def pre_nms_top_n(self):
        if self.training:
            return self._pre_nms_top_n['training']
        return self._pre_nms_top_n['testing']

    @property
    def post_nms_top_n(self):
        if self.training:
            return self._post_nms_top_n['training']
        return self._post_nms_top_n['testing']

    def create_proposals(self, pred_bbox_deltas, anchors):
        proposals = self.box_coder.decode(pred_bbox_deltas.detach(), anchors)
        return proposals

    def filter_proposals(self, proposals, objectness, num_anchors_per_lvl, image_size):
        objectness = objectness.detach()
        levels = []
        for idx, n in enumerate(num_anchors_per_lvl):
            levels.append(torch.full((n,), idx, dtype=torch.int64, device = proposals.device))
        levels = torch.cat(levels, dim = 0)
        levels = levels.reshape(1, -1).expand_as(objectness)

        final_boxes = []
        final_scores = []
        for i, (boxes, scores, level) in enumerate(zip(proposals, objectness, levels)):
            boxes = self.box_coder.clip_boxes_to_image(boxes, image_size)
            keep = self.box_coder.remove_small_boxes(boxes, self.min_size)
            boxes, scores, level = boxes[keep], scores[keep], level[keep]
            # non-maximum suppression, independently done per level
            keep = self.box_coder.batched_nms(boxes, scores, level, self.nms_thresh)
            # keep only topk scoring predictions
            keep = keep[:self.post_nms_top_n]
            boxes, scores = boxes[keep], scores[keep]
            # add padding to match shape and append
            final_boxes.append(F.pad(boxes, [0, self.post_nms_top_n - keep.numel(), 0, 0], 'constant', 0.0))
            final_scores.append(F.pad(scores, [0, self.post_nms_top_n - keep.numel()], 'constant', 0.0))

        final_boxes = torch.stack(final_boxes, dim = 0)
        final_scores = torch.stack(final_scores, dim = 0)
        return final_boxes, final_scores

    @staticmethod
    def assign_targets(target_matcher, proposals, bboxes, labels):
        matches = target_matcher(proposals, bboxes)
        # get the targets corresponding GT for each proposal
        # NB: need to clamp the indices because we can have a single
        # GT in the image, and matched_idxs can be -2, which goes
        # out of bounds
        batch_idx = torch.arange(bboxes.size(0))[:, None]
        gt_proposals = bboxes[batch_idx, matches.clamp(min = 0)]
        # Foreground (positive examples)
        gt_labels = labels[batch_idx, matches.clamp(min = 0)]
        gt_labels[matches.eq(target_matcher.BETWEEN_THRESHOLDS)] = -1
        # Background (negative examples)
        gt_labels[matches.eq(target_matcher.BELOW_LOW_THRESHOLD)] = 0
        return gt_proposals, gt_labels

    def forward(self, pred_bbox_deltas, objectness, anchors, num_anchors_per_lvl, image_size):
        proposals = self.create_proposals(pred_bbox_deltas, anchors)
        proposals, objectness = self.filter_proposals(proposals, objectness, num_anchors_per_lvl, image_size)
        return proposals, objectness
