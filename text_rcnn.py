import torch
from torch import nn
from backbone import Backbone
from anchor import AnchorGenerator
from collections import OrderedDict
from poolers import MultiScaleRoIAlign
from rpn import RPNHead, RegionProposalNetwork
from roi_heads import BoxHead, BoxPredictor, RoIHeads
from seq_heads import TextHead, TextPredictor, SeqHeads

class TextRCNN(nn.Module):
    def __init__(self, backbone = None,
                 # RPN parameters
                 rpn_anchor_generator = None, rpn_head = None,
                 rpn_pre_nms_top_n_train = 2000, rpn_pre_nms_top_n_test = 1000,
                 rpn_post_nms_top_n_train = 2000, rpn_post_nms_top_n_test = 1000,
                 rpn_nms_thresh = 0.7,
                 rpn_fg_iou_thresh = 0.7, rpn_bg_iou_thresh = 0.3,
                 rpn_num_samples = 256, rpn_positive_fraction = 0.5,
                 # Box parameters
                 box_roi_pool = None, box_head = None, box_predictor = None,
                 box_score_thresh = 0.05, box_nms_thresh = 0.5, box_detections_per_img = 32,
                 box_fg_iou_thresh = 0.5, box_bg_iou_thresh = 0.5,
                 box_num_samples = 128, box_positive_fraction = 0.25,
                 bbox_reg_weights = None, num_classes = None,
                 # Text parameters
                 text_roi_pool = None, text_head = None, text_predictor = None,
                 text_nms_thresh = 0.1, texts_per_img = 32, num_chars = None,
                 text_fg_iou_thresh = 0.9, text_bg_iou_thresh = 0.9,  blank = 0,
                 text_num_samples = 64, text_positive_fraction = 1.0, input_length = None):

        super(TextRCNN, self).__init__()

        if backbone is None:
            backbone = Backbone(True)

        if not hasattr(backbone, "out_channels"):
            raise ValueError("backbone should contain an attribute out_channels")

        assert isinstance(rpn_anchor_generator, (AnchorGenerator, type(None)))
        assert isinstance(box_roi_pool, (MultiScaleRoIAlign, type(None)))

        if num_classes is not None:
            if box_predictor is not None:
                raise ValueError("num_classes should be None when box_predictor is specified")
        else:
            if box_predictor is None:
                raise ValueError("num_classes should not be None when box_predictor is not specified")

        if input_length is not None:
            if text_head is not None:
                raise ValueError("input_length should be None when text_head is specified")
        else:
            if text_head is None:
                raise ValueError("input_length should not be None when text_head is not specified")

        if num_chars is not None:
            if text_predictor is not None:
                raise ValueError("num_chars should be None when text_predictor is specified")
        else:
            if text_predictor is None:
                raise ValueError("num_chars should not be None when text_predictor is not specified")

        out_channels = backbone.out_channels
        self.backbone = backbone

        if rpn_anchor_generator is None:
            anchor_sizes = ((32,), (64,), (128,), (256,), (512,))
            aspect_ratios = ((0.5, 1.0, 2.0),) * len(anchor_sizes)
            rpn_anchor_generator = AnchorGenerator(anchor_sizes, aspect_ratios)

        if rpn_head is None:
            rpn_head = RPNHead(out_channels, rpn_anchor_generator.num_anchors_per_location()[0])

        rpn_pre_nms_top_n = dict(training = rpn_pre_nms_top_n_train, testing = rpn_pre_nms_top_n_test)
        rpn_post_nms_top_n = dict(training = rpn_post_nms_top_n_train, testing = rpn_post_nms_top_n_test)

        self.rpn = RegionProposalNetwork(
            rpn_anchor_generator, rpn_head,
            rpn_fg_iou_thresh, rpn_bg_iou_thresh,
            rpn_num_samples, rpn_positive_fraction,
            rpn_pre_nms_top_n, rpn_post_nms_top_n, rpn_nms_thresh)

        if box_roi_pool is None:
            box_roi_pool = MultiScaleRoIAlign(featmap_names = [0, 1, 2, 3], output_size = 7, sampling_ratio = 2)

        if box_head is None:
            resolution = box_roi_pool.output_size[0]
            representation_size = 1024
            box_head = BoxHead(out_channels * resolution ** 2, representation_size)

        if box_predictor is None:
            representation_size = 1024
            box_predictor = BoxPredictor(representation_size, num_classes)

        self.roi_heads = RoIHeads(
            # Box
            box_roi_pool, box_head, box_predictor,
            box_fg_iou_thresh, box_bg_iou_thresh,
            box_num_samples, box_positive_fraction,
            bbox_reg_weights,
            box_score_thresh, box_nms_thresh, box_detections_per_img)

        if text_roi_pool is None:
            text_roi_pool = MultiScaleRoIAlign(featmap_names = [0, 1, 2, 3], output_size = 7, sampling_ratio = 2)

        if text_head is None:
            resolution = text_roi_pool.output_size[0]
            text_head = TextHead(resolution ** 2, input_length)

        if text_predictor is None:
            text_predictor = TextPredictor(out_channels, num_chars, hidden_size = 512, n_layers = 1, bidirectional = True)

        self.seq_heads = SeqHeads(
            # Text
            text_roi_pool, text_head, text_predictor,
            text_fg_iou_thresh, text_bg_iou_thresh,
            text_num_samples, text_positive_fraction,
            text_nms_thresh, texts_per_img, blank)

    def forward(self, images, bboxes = None, labels = None):
        feature_maps = self.backbone(images)

        if isinstance(feature_maps, torch.Tensor):
            feature_maps = OrderedDict([(0, feature_maps)])
        proposals_manager = self.rpn(images, feature_maps)

        proposed_boxes = proposals_manager['boxes']
        if bboxes is not None:
            proposed_boxes = torch.cat((proposed_boxes, bboxes.to(proposed_boxes)), dim = 1)
        detections_manager = self.roi_heads(feature_maps, proposed_boxes, images.shape[-2:])

        detected_boxes = detections_manager['boxes']
        class_scores = detections_manager['scores']
        class_labels = detections_manager['labels']
        if bboxes is not None and labels is not None:
            detected_boxes = torch.cat((bboxes.to(detected_boxes), detected_boxes), dim = 1)
            class_scores = torch.cat((labels.gt(0).to(class_scores), class_scores), dim = 1)
            class_labels = torch.cat((labels.to(class_labels, class_labels)), dim = 1)
        sequence_manager = self.seq_heads(feature_maps, detected_boxes, class_scores, class_labels, images.shape[-2:])
        return proposals_manager, detections_manager, sequence_manager
