import torch
from torch import nn
from utils import BoxCoder
import torch.nn.functional as F

class TextDecoder(nn.Module):
    def __init__(self, nms_thresh, texts_per_img, blank):
        super(TextDecoder, self).__init__()
        self.texts_per_img = texts_per_img
        self.nms_thresh = nms_thresh
        self.blank = blank

    def decode_texts(self, char_logits):
        texts = char_logits.max(dim = -1, keepdim = False)[1].permute(1, 0) # [B * N, S]
        decoded_texts = []
        for text in texts:
            text = [v for j, v in enumerate(text) if (j == 0 or v != text[j - 1]) and v != self.blank]
            text += [self.blank] * (char_logits.size(0) - len(text))
            decoded_texts.append(text)
        decoded_texts = torch.tensor(decoded_texts).to(texts)
        return decoded_texts

    def postprocess_texts(self, pred_texts, detections, class_scores, class_labels):
        pred_texts = pred_texts.reshape(*detections.shape[:2], -1)

        final_boxes = []
        final_scores = []
        final_labels = []
        final_texts = []
        for boxes, scores, labels, texts in zip(detections, class_scores, class_labels, pred_texts):
            # keep only topk scoring predictions
            keep = torch.nonzero(labels.gt(0)).squeeze(1)[:self.texts_per_img]
            boxes, scores, labels, texts = boxes[keep], scores[keep], labels[keep], texts[keep]
            # non-maximum suppression, independently done per class
            keep = BoxCoder.batched_nms(boxes, scores, labels, self.nms_thresh)
            boxes, scores, labels, texts = boxes[keep], scores[keep], labels[keep], texts[keep]
            # add padding to match shape and append
            final_boxes.append(F.pad(boxes, [0, self.texts_per_img - keep.numel(), 0, 0], 'constant', 0.0))
            final_scores.append(F.pad(scores, [0, self.texts_per_img - keep.numel()], 'constant', 0.0))
            final_labels.append(F.pad(labels, [0, self.texts_per_img - keep.numel()], 'constant', 0))
            final_texts.append(F.pad(texts, [0, self.texts_per_img - keep.numel()], 'constant', self.blank))
        
        final_boxes = torch.stack(final_boxes, dim = 0)
        final_scores = torch.stack(final_scores, dim = 0)
        final_labels = torch.stack(final_labels, dim = 0)
        final_texts = torch.stack(final_texts, dim = 0)
        return final_boxes, final_scores, final_labels, final_texts

    def forward(self, char_logits, detections, scores, labels):
        texts = self.decode_texts(char_logits.detach())
        detections, scores, labels, texts = self.postprocess_texts(texts, detections, scores, labels)
        return detections, scores, labels, texts
