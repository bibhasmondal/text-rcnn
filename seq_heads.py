import torch
from torch import nn
from decoder import TextDecoder
from utils import Matcher, BalancedPositiveNegativeSampler, TextManager

class TextHead(nn.Module):
    def __init__(self, in_channels, input_length):
        super(TextHead, self).__init__()
        self.fc6 = nn.Linear(in_channels, input_length)
        self.fc7 = nn.Linear(input_length, input_length)

    def forward(self, x):
        x = x.flatten(start_dim = -2)
        x = torch.relu(self.fc6(x))
        x = torch.relu(self.fc7(x))
        return x

class TextPredictor(nn.Module):
    def __init__(self, input_size, num_char, hidden_size,
                 n_layers, bidirectional):

        super(TextPredictor, self).__init__()
        self.layer1 = nn.LSTM(input_size, hidden_size, n_layers, bidirectional = bidirectional)
        self.layer2 = nn.LSTM(input_size, hidden_size, n_layers, bidirectional = bidirectional)
        self.layer3 = nn.LSTM(2 * hidden_size, hidden_size, n_layers, bidirectional = bidirectional)
        self.layer4 = nn.LSTM(2 * hidden_size, hidden_size, n_layers, bidirectional = bidirectional)
        self.layer5 = nn.Linear(hidden_size * 4, hidden_size * 4)
        self.layer6 = nn.Linear(hidden_size * 4, num_char)

    def forward(self, x):
        output = x.flatten(start_dim = 0, end_dim = -3) # [B * N, C, S]
        output = output.permute(2, 0, 1) # [S, B * N, C]
        output1, _ = self.layer1(output)
        output2, _ = self.layer2(output)
        output = output1 + output2
        output1, _ = self.layer3(output)
        output2, _ = self.layer4(output)
        output = torch.cat([output1, output2],dim = 2)
        output = self.layer5(output)
        output = self.layer6(output)
        output = output
        return output

class SeqHeads(nn.Module):
    def __init__(self, text_roi_pool,
                 text_head, text_predictor,
                 # training
                 fg_iou_threshold, bg_iou_threshold,
                 num_samples, positive_fraction,
                 # inference
                 nms_thresh, texts_per_img, blank):

        super(SeqHeads, self).__init__()
        self.text_roi_pool = text_roi_pool
        self.text_head = text_head
        self.text_predictor = text_predictor
        # used during training
        self.target_matcher = Matcher(fg_iou_threshold, bg_iou_threshold, allow_low_quality_matches = False)
        self.fg_bg_sampler = BalancedPositiveNegativeSampler(num_samples, positive_fraction)
        self.text_decoder = TextDecoder(nms_thresh, texts_per_img, blank)

    def forward(self, feature_maps, detections, class_scores, class_labels, image_size):
        text_features = self.text_roi_pool(feature_maps, detections, image_size)
        text_features = self.text_head(text_features)
        char_logits = self.text_predictor(text_features)
        detection_boxes, scores, labels, texts = self.text_decoder(char_logits, detections, class_scores, class_labels)
        kwargs = {
            'processed_data': dict(boxes = detection_boxes, scores = scores, labels = labels, texts = texts),
            'char_logits': char_logits,
            'ref_boxes': detections,
        }
        sequence_manager = TextManager(self.target_matcher, self.fg_bg_sampler, self.text_decoder.blank, **kwargs)
        return sequence_manager
