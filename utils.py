import math
import torch
import torch.nn.functional as F
from torchvision.ops import nms
from anchor import AnchorGenerator
from proposal import ProposalCreator
from detection import DetectionMaker

class BalancedPositiveNegativeSampler(object):
    def __init__(self, num_samples, pos_ratio):
        self.num_samples = num_samples
        self.pos_ratio = pos_ratio

    def __call__(self, matches_mask):
        pos_idx_mask = []
        neg_idx_mask = []
        for match_mask in matches_mask:
            pos_inds = torch.nonzero(match_mask >= 1).squeeze(1)
            neg_inds = torch.nonzero(match_mask == 0).squeeze(1)
            num_pos = int(self.num_samples * self.pos_ratio)
            # protect against not enough positive examples
            num_pos = min(pos_inds.numel(), num_pos)
            num_neg = self.num_samples - num_pos
            # protect against not enough negative examples
            num_neg = min(neg_inds.numel(), num_neg)
            num_pos = self.num_samples - num_neg
            # randomly select positive and negative examples
            pos_params = torch.randperm(pos_inds.numel(), device=pos_inds.device)[:num_pos]
            neg_params = torch.randperm(neg_inds.numel(), device=neg_inds.device)[:num_neg]
            # create binary mask from indices
            pos_mask = torch.zeros_like(match_mask, dtype=torch.bool)
            neg_mask = torch.zeros_like(match_mask, dtype=torch.bool)
            pos_mask[pos_inds[pos_params]] = True
            neg_mask[neg_inds[neg_params]] = True
            # Append to list
            pos_idx_mask.append(pos_mask)
            neg_idx_mask.append(neg_mask)
        # To tensor
        pos_idx_mask = torch.stack(pos_idx_mask, dim = 0)
        neg_idx_mask = torch.stack(neg_idx_mask, dim = 0)
        return pos_idx_mask, neg_idx_mask

class Matcher(object):
    BELOW_LOW_THRESHOLD = -2
    BETWEEN_THRESHOLDS = -1
    def __init__(self, high_threshold, low_threshold, allow_low_quality_matches=False):
        assert low_threshold <= high_threshold
        self.high_threshold = high_threshold
        self.low_threshold = low_threshold
        self.allow_low_quality_matches = allow_low_quality_matches

    def __call__(self, pred_boxes, gt_boxes):
        match_quality_matrix = BoxCoder.box_iou(pred_boxes, gt_boxes)
        # match_quality_matrix is B(batch) x P (predicted) x G (gt) 
        # Max over gt elements (dim 2) to find best gt candidate for each prediction
        matched_vals, matches = match_quality_matrix.max(dim = -1)
        if self.allow_low_quality_matches:
            matched_vals[self.low_quality_matches_indices(match_quality_matrix)] = self.high_threshold
        # Assign candidate matches with low quality to negative (unassigned) values
        below_low_threshold = matched_vals < self.low_threshold
        between_thresholds = (matched_vals >= self.low_threshold) & (matched_vals < self.high_threshold)
        matches[below_low_threshold] = Matcher.BELOW_LOW_THRESHOLD
        matches[between_thresholds] = Matcher.BETWEEN_THRESHOLDS
        return matches

    def low_quality_matches_indices(self, match_quality_matrix):
        # For each gt, find the prediction with which it has highest quality
        highest_quality_foreach_gt, _ = match_quality_matrix.max(dim = -2)
        # Find highest quality match available, even if it is low, including ties
        inds = torch.sum(match_quality_matrix == highest_quality_foreach_gt[..., None, :], dim = -1, dtype = torch.bool)
        return inds

class BoxCoder(object):
    def __init__(self, weights = (1., 1., 1., 1.), bbox_xform_clip = math.log(1000. / 16)):
        self.weights = weights
        self.bbox_xform_clip = bbox_xform_clip

    def decode(self, bbox_deltas, src_bboxes):
        src_width = src_bboxes[..., 2] - src_bboxes[..., 0]
        src_height = src_bboxes[..., 3] - src_bboxes[..., 1]
        src_ctr_x = src_bboxes[..., 0] + 0.5 * src_width
        src_ctr_y = src_bboxes[..., 1] + 0.5 * src_height

        dx = bbox_deltas[..., 0::4] / self.weights[0]
        dy = bbox_deltas[..., 1::4] / self.weights[1]
        dw = bbox_deltas[..., 2::4] / self.weights[2]
        dh = bbox_deltas[..., 3::4] / self.weights[3]
        # Prevent sending too large values into torch.exp()
        dw = torch.clamp(dw, max = self.bbox_xform_clip)
        dh = torch.clamp(dh, max = self.bbox_xform_clip)

        dst_ctr_x = dx * src_width[..., None] + src_ctr_x[..., None]
        dst_ctr_y = dy * src_height[..., None] + src_ctr_y[..., None]
        dst_width = torch.exp(dw) * src_width[..., None]
        dst_height = torch.exp(dh) * src_height[..., None]

        dst_bboxes = torch.zeros_like(bbox_deltas)
        dst_bboxes[..., 0::4] = dst_ctr_x - 0.5 * dst_width
        dst_bboxes[..., 1::4] = dst_ctr_y - 0.5 * dst_height
        dst_bboxes[..., 2::4] = dst_ctr_x + 0.5 * dst_width
        dst_bboxes[..., 3::4] = dst_ctr_y + 0.5 * dst_height
        return dst_bboxes

    def encode(self, src_bboxes, dst_bboxes):
        src_width = src_bboxes[..., 2] - src_bboxes[..., 0]
        src_height = src_bboxes[..., 3] - src_bboxes[..., 1]
        src_ctr_x = src_bboxes[..., 0] + 0.5 * src_width
        src_ctr_y = src_bboxes[..., 1] + 0.5 * src_height

        dst_width = dst_bboxes[..., 2] - dst_bboxes[..., 0]
        dst_height = dst_bboxes[..., 3] - dst_bboxes[..., 1]
        dst_ctr_x = dst_bboxes[..., 0] + 0.5 * dst_width
        dst_ctr_y = dst_bboxes[..., 1] + 0.5 * dst_height

        dx = self.weights[0] * (dst_ctr_x - src_ctr_x) / src_width
        dy = self.weights[1] * (dst_ctr_y - src_ctr_y) / src_height
        dw = self.weights[2] * torch.log(dst_width / src_width)
        dh = self.weights[3] * torch.log(dst_height / src_height)
        bbox_deltas = torch.stack((dx, dy, dw, dh), dim = -1)
        return bbox_deltas

    @staticmethod
    def box_area(boxes):
        return (boxes[..., 2] - boxes[..., 0]) * (boxes[..., 3] - boxes[..., 1])

    @staticmethod
    def remove_small_boxes(boxes, min_size):
        ws, hs = boxes[:, 2] - boxes[:, 0], boxes[:, 3] - boxes[:, 1]
        keep = (ws >= min_size) & (hs >= min_size)
        keep = keep.nonzero().squeeze(1)
        return keep

    @staticmethod
    def clip_boxes_to_image(boxes, size):
        dim = boxes.dim()
        boxes_x = boxes[..., 0::2]
        boxes_y = boxes[..., 1::2]
        height, width = size
        boxes_x = boxes_x.clamp(min=0, max=width)
        boxes_y = boxes_y.clamp(min=0, max=height)
        clipped_boxes = torch.stack((boxes_x, boxes_y), dim=dim)
        return clipped_boxes.reshape(boxes.shape)

    @staticmethod
    def batched_nms(boxes, scores, idxs, iou_threshold):
        if boxes.numel() == 0:
            return torch.empty((0,), dtype=torch.int64, device=boxes.device)
        # strategy: in order to perform NMS independently per class.
        # we add an offset to all the boxes. The offset is dependent
        # only on the class idx, and is large enough so that boxes
        # from different classes do not overlap
        max_coordinate = boxes.max()
        offsets = idxs.to(boxes) * (max_coordinate + 1)
        boxes_for_nms = boxes + offsets[:, None]
        keep = nms(boxes_for_nms, scores, iou_threshold)
        return keep

    @staticmethod
    def box_iou(pred_boxes, gt_boxes):
        pred_area = BoxCoder.box_area(pred_boxes)
        gt_area =BoxCoder.box_area(gt_boxes)

        lt = torch.max(pred_boxes[..., None, :2], gt_boxes[..., None, :, :2])  # [B, P, G, 2]
        rb = torch.min(pred_boxes[..., None, 2:], gt_boxes[..., None, :, 2:])  # [B, P, G, 2]

        wh = (rb - lt).clamp(min=0)  # [B, P, G, 2]
        inter = wh[..., 0] * wh[..., 1]  # [B, P, G]

        iou = inter / (pred_area[..., None] + gt_area[..., None, :] - inter)
        return iou

class LevelMapper(object):
    def __init__(self, k_min, k_max, canonical_scale = 224, canonical_level = 4, eps = 1e-6):
        self.k_min = k_min
        self.k_max = k_max
        self.s0 = canonical_scale
        self.lvl0 = canonical_level
        self.eps = eps

    def __call__(self, boxes):
        # Compute level ids
        s = torch.sqrt(BoxCoder.box_area(boxes))
        # Eqn.(1) in FPN paper
        target_lvls = torch.floor(self.lvl0 + torch.log2(s / self.s0 + self.eps))
        target_lvls = torch.clamp(target_lvls, min=self.k_min, max=self.k_max)
        return target_lvls.to(torch.int64) - self.k_min

class BoxManager(object):
    def __init__(self, target_matcher, fg_bg_sampler, box_coder,
                 src_processed_data, src_deltas, src_scores,
                 dst_boxes):

        self.target_matcher = target_matcher
        self.fg_bg_sampler = fg_bg_sampler
        self.box_coder = box_coder
        self.src_processed_data = src_processed_data
        self.src_deltas = src_deltas
        self.src_scores = src_scores
        self.dst_boxes = dst_boxes

    def __getitem__(self, key):
        return self.src_processed_data.get(key)

    def loss(self, bboxes, labels = None):
        if labels is None:
            # ************ RPN Loss *************
            gt_boxes, gt_labels = AnchorGenerator.assign_targets(self.target_matcher, self.dst_boxes, bboxes)
            src_deltas = self.src_deltas.repeat(1, 1, 2).reshape(*gt_boxes.shape[:2], -1, 4)
            box_loss_func = F.l1_loss
            label_loss_func = F.binary_cross_entropy_with_logits
            class_dtype = torch.float
        else:
            # ************ Box Loss *************
            gt_boxes, gt_labels = ProposalCreator.assign_targets(self.target_matcher, self.dst_boxes, bboxes, labels)
            src_deltas = self.src_deltas.repeat(1, 1, 1).reshape(*gt_boxes.shape[:2], -1, 4)
            box_loss_func = F.smooth_l1_loss
            label_loss_func = F.cross_entropy
            class_dtype = torch.long
        # Balance pos neg sample
        pos_idx_mask, neg_idx_mask = self.fg_bg_sampler(gt_labels)
        sampled_inds = pos_idx_mask | neg_idx_mask
        # Filter ssampled indices
        pred_bbox_deltas = src_deltas[pos_idx_mask, gt_labels[pos_idx_mask]]
        gt_bbox_deltas = self.box_coder.encode(self.dst_boxes[pos_idx_mask], gt_boxes[pos_idx_mask])

        pred_labels = self.src_scores[sampled_inds]
        gt_class = gt_labels[sampled_inds]
        # Box loss
        box_loss = box_loss_func(pred_bbox_deltas, gt_bbox_deltas, reduction="sum") / sampled_inds.sum()
        # Label loss
        label_loss = label_loss_func(pred_labels, gt_class.to(class_dtype))
        return box_loss, label_loss

class TextManager(object):
    def __init__(self, target_matcher, fg_bg_sampler, blank, 
                 processed_data, char_logits, ref_boxes):

        self.target_matcher = target_matcher
        self.fg_bg_sampler = fg_bg_sampler
        self.blank = blank

        self.processed_data = processed_data
        self.char_logits = char_logits
        self.ref_boxes = ref_boxes

    def __getitem__(self, key):
        return self.processed_data.get(key)

    def loss(self, bboxes, text):
        gt_labels, gt_text = DetectionMaker.assign_targets(self.target_matcher, self.ref_boxes, bboxes, text)
        # Balance pos neg sample
        pos_idx_mask, _ = self.fg_bg_sampler(gt_labels)
        # Sample
        char_logits = self.char_logits[:, pos_idx_mask.reshape(-1)]
        gt_text = gt_text[pos_idx_mask]
        # CTC loss
        output_len = gt_text.ge(self.blank).sum(dim = -1).reshape(-1)
        input_len = torch.full(output_len.shape, char_logits.size(0)).to(output_len)
        num = max(gt_text.numel(), 1e-7)
        loss = F.ctc_loss(char_logits, gt_text, input_len, output_len, blank = self.blank, reduction = 'sum') / num
        return loss
